#!/usr/bin/env node
/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require */
'use strict';
/**
 * sampson test runner harness
 * @module scripts/test
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires child_process
 * @requires module:mout/lang/clone
 * @requires fs
 * @requires path
 * @requires os
 * @requires util
 */

var child_process = require('child_process')               // child proces for spawning mocha
  , clone         = require('mout/lang/clone')             // object clone module
  , fs            = require('fs')                          // fs module
  , path          = require('path')                        // path module
  , os            = require('os')                          // os module
  , util          = require("util")                        // util module
  , production    = (process.env.NODE_ENV == 'production') // boolean flag if we are in producion mode
  , env           = clone( process.env )                   // clone of current process env
  , debug         = require('debug')( 'scripts:runner')
  , npath         = ( env.NODE_PATH || "" ).split( path.delimiter )     // cache of node path split into an array
  , html                                                   // html stream
  , coverage                                               // mocha code coverage process
  , mocha                                                  // moacha child process
  , reporter
  , spinner

var conf = require('hive-conf')


// add our modules director to node require path
// so we don't have to require( ../../../../../ )
npath.push(path.resolve(__dirname,'..','packages') )
npath = npath.join( path.delimiter )
env.NODE_PATH = npath  

// inject some test vars into the env
// for the conf loader to read / test

//env.databases__commerce= env.databases__commerce || 'mongodb://localhost:27017/test';
// env.DEBUG='alice:commands:import'
// set a conf director for the conf tests


reporter = process.stdout


// spin up mocha configured the way I want it
var mocha = child_process.spawn("mocha", [
    "--harmony"
    , "--growl"
    , "--recursive"
    ,"--timeout=10000"
    , util.format("--reporter=%s", production ? 'xunit':'spec')
    , "packages/**/test/*.js"
    ], { env:env });

    mocha.on('exit', function( code, sig ){
    process.exit( code );
});

mocha.stdout.pipe( reporter )
mocha.stderr.pipe( reporter )
