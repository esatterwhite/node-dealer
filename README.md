Running on [heroku](http://node-dealer-dev.herokuapp.com/)

### Installation

* Clone project
* Install via npm `npm install` 
* Optionally set up cli `npm link`

### Running
The simplest way to start the app is change any config settings in `hive.json` and start the web app with npm 

```
npm start
```
Alternatively, it can be run through the `dealer` cli ( which is installed w/ `npm link` ) as it sets up package path configuration for you.

* running at http://0.0.0.0:3000 by default

```
dealer web --help
dealer web -l stdout -p 3000
```

You can rund the cli directly just the same

```
node ./bin/cli.js web
node ./bin/cli.js web -l stdout -p 3000
```




### Configuration

Configuration for the app can be done by:

* Modifying the `hive.json` file
* passing cli flags
* setting environment variables

#### Configuring a Database

By default the application will try to run on mongodb's default settings of localhost:27017 in the `dealer` database

`host`, `port` ,`user` and `password` can be set in hive.json.

Alternatively, they can be set through `env` variables or `cli` flags.

##### ENV Variables

```
dealer__databases__dealer__host=<HOST> dealer__databases__dealer__port=<PORT> node ./bin/cli.js web
```

##### CLI Flags
```
node ./bin/cli.js web --dealer:databases:dealer:host=<HOST> --dealer:databases:dealer:port=<PORT>
```

###### As a connection string

Rather than sending individual parameters, you can seend a fully qualified mongodb connection string 

```
node ./bin/cli.js web --dealer:databases:dealer=mongodb://localhost:27017/dealer
```

#### Setting log levels
You can attach 0 or more loggers to the running application though the `logger` config option. The supported default types are `stdout`, `file` and `syslog`

```
dealer web -l stdout
dealer web -l stdout --logger=file

logger=stdout dealer web
logger=stdout node ./bin/cli.js web -p 5000

logger=stdout npm start
```

each log type has its set of coniguration options. Nested option values can be accessed by using `:` as a separator when using CLI flags, or with `__` when using env vars.

```
log__stdout__level=warning dealer web -p 3000
PORT=3000 dealer web --log:stdout:level=warning
```

http://0.0.0.0:3000