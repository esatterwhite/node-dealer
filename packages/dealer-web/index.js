/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * index.js
 * @module index.js
 * @author Eric Satterwhite
 * @since 0.0.1
 */

var express = require( 'express' )
  , cookieParser = require('cookie-parser')
  , logger = require('hive-log')
  , conf   = require('hive-conf')
  , morgan = require('morgan')
  , path = require('path')
  , guid = require('hive-stdlib/random').guid
  , resources = require('dealer-web/lib/loading/resources')
  , session = require('express-session')
  , app
  ;

app = express();
app.set( 'query parser', 'extended' );
app.set('x-powered-by', false);
app.use( cookieParser() );
app.use(
    session({
    genid: function(req) {
      return guid()
    },
    secret:conf.get('hive:secret'),
    name:'dealer_session_id',
    secure: false,
    resave: true,
    cookie:{
      maxAge:5000000000,
      httpOnly: false
    }
  })
);
app.use( '/static', express.static( path.join(__dirname, 'static' ) ) )
app.use( '/static', express.static( path.join(__dirname, 'bower_components' ) ) )
app.get('/', function(req, res, next){
	res.sendFile(path.join(__dirname,'index.html'));
});
app.use(
   morgan(':remote-addr - :remote-user [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ( :response-time ms )',{
        stream:{
         write: function( msg, encoding ){
                logger.http( msg )
            }
        }
    })
);

var v1 = new express.Router();
resources
	.load()
	.flat()
	.forEach(function( endpoint ){
		v1[endpoint.method]( endpoint.route, endpoint.handler )
	});

app.use('/api/v1', v1 );

module.exports = app;

if (require.main === module) {
	app.listen(conf.get('PORT'), '0.0.0.0', function(){
		logger.info("Server running at http://0.0.0.0:" + conf.get("PORT"))
	})
}
