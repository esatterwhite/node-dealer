/*jshint laxcomma: true, smarttabs: true, node: true */
'use strict';
/**
 * Loader instance to locate resource definitions
 * @module dealer-web/lib/loading/resources
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires dealer-core/lib/loading/loader
 * @requires hive-stdlib/class
 * @requires hive-stdlib/object/values
 * @requires hive-stdlib/array/flatten
 */

var Loader  = require( 'dealer-core/lib/loading/loader' )
  , Class   = require('hive-stdlib/class')
  , values  = require('hive-stdlib/object').values
  , flatten = require("hive-stdlib/array").flatten
  , Resource
  , loader
  ;

/**
 * @extends module:dealer-core/lib/loading/loader
 * @class module:dealer-web/lib/loading/resources.Loader
 * @param {Object} [options]
 * @param {String} [searchpath=resource] relative folder name to search through
 * @param {RegExp} [filepattern=/\.js$/] a file name pattern to search for
 */
Resource = new Class({
	inherits: Loader
	, options:{
		searchpath:'resources'
		,filepattern:/\.js$/
	}
	,remap: function remap( loaded ){
		var resources = this.parent('remap', loaded );

		return resources && values( resources )
	}

	/**
	 * The method used to flatten out the loader data structure to a single array
	 * @method module:dealer-web/lib/loading/resources.Loader#flat
	 * @return {Array} A single arry of the loaded files or modules
	 **/
	, flat: function flat(){
		var items = values( this )
		  , out = []
		  , next;
		  ;

		for(var x=0,len=items.length; x<len; x++){
			next = items[x];
			if( Array.isArray( next ) ){
				out.push.apply( out, next );
			}else{
				out = out.concat( next );
			}
		}
		return flatten( out );
	}
});

loader = new Resource();

/**
 * Locates all resource file locations
 * @return {Array} array of file references that were located
 **/
exports.find = function(){
	return loader.find.apply( loader, arguments );
}

/**
 * loads all located files through `require`
 * @return {Array} Array of loaded resource objets
 **/
exports.load = function( ){
	return loader.load.apply( loader, arguments )
};

exports.Loader = Resource;

