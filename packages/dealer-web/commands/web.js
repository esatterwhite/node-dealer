/*jshint laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * web.js
 * @module web.js
 * @author 
 * @since 0.0.1
 * @requires seeli
 */

var cli = require( 'seeli' )
  , path = require('path')
  , child_process = require('child_process')
  , clone         = require('../../../node_modules/hive-stdlib/lang').clone             // object clone module
  , env           = clone( process.env )
  , npath         = ( env.NODE_PATH || "" ).split( path.delimiter )     // cache of node path split into an array
  ;

  npath.push( path.resolve(__dirname,'..','..') )
  npath = npath.join( path.delimiter )
  env.NODE_PATH = npath  


  function shutdown(){
	process.exit();
  }

/**
 * Description
 * @class module:web.js.Thing
 * @param {TYPE} param
 * @example var x = new web.js.THING();
 */

module.exports = new cli.Command({
	usage:[
		cli.bold("Usage: ") + "dealer web -p 6000 --logger=stdout"
	]
	,description:"starts the web server"
	,run: function( directive, data, done ){
		var proc = child_process.fork( path.join(__dirname,'..', 'index.js')
			,process.argv.slice(3)
			,{env:env, stdio: 'inherit'}
		);

		proc.on( 'exit', shutdown );
		proc.on('error', shutdown );

		// command is never done.
		// so... don't call done.
	}
})
