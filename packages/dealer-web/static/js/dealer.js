

angular
	.module('dealer',['mm.foundation', 'ngCookies', 'ngResource', 'angularMoment'])
	.factory('DeckHistory', ['$resource','$cookies', function($resource, $cookies){
		console.log()
		return $resource('api/v1/deck/history/:session_id', {
			session_id:'@session_id'
		}, {
		  query: {
		  	method:'GET'
		  	, isArray:false
		  }
		});
	}])
	
	.controller('DealerCtl', function( $scope, $http, $cookies, DeckHistory ){
		function makerows( cards ){
			var rows = []
			  , idx = 0;

		  while( idx < 4 ){
		  	rows.push( cards.slice( idx * 13 + 0, idx* 13 + 13  ) )
		  	idx++;
		  }

		  return rows;
		}

		function refresh( data, status, headers, config ){
			$http.get('/api/v1/deck')
			  .success(function(data, status, headers, config) {
			    $scope.deck = data;
			    $scope.score = Math.round( ( data.score / 104 )  * 100 )
				$scope.average = Math.round( data.average )
			    $scope.history = DeckHistory.query({session_id:$cookies.get('dealer_session_id').split(/s:([\w-]+)\./)[1]});
				$scope.suits = makerows( data.cards )
			  })

			  .error(function(data, status, headers, config) {
			    // log error
			  });
		}

		$scope.loadhistory = function( deck ){
			$scope.score = Math.round( ( deck.score / 104 )  * 100 )
			$scope.average = Math.round( deck.score_pct )
			$scope.suits = makerows( deck.cards );
		};

		$scope.refresh = refresh;
		refresh();
	})

	.controller('HistoryCtl', function( $scope, $http, $interval,$cookies, $timeout, DeckHistory ){
		$scope.now = new Date();

		$scope.clearhistory = function(){
			$http
				.get('/api/v1/ping')
				.success(function(){
					$timeout(function(){
						$scope.history = DeckHistory.query({session_id:$cookies.get('dealer_session_id').split(/s:([\w-]+)\./)[1]});
					},150)
				})
		};
		
		$interval(function(){
			$scope.now = new Date();
		},60 * 1000 );

	});
