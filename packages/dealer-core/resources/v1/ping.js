/*jshint laxcomma: true, smarttabs: true, node:true*/
'use strict';
/**
 * resource to reset end user session AKA clearing history
 * @module dealer-web/resources/v1/ping
 * @author Eric Satterwhite
 * @since 0.0.1
 */

exports.get_ping = {
	method:'get'
	,route:"/ping"
	,handler: function( req, res, next){
		req.session.regenerate(function( err ){
			return res.send({
				success:true,
				status:200
			});
		})
	}
}
