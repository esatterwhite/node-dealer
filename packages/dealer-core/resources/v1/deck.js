/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Primary resources for dealer game
 * @module dealer-core/resources/v1/deck
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires hive-stdlib/random/guid
 * @requires dealer-core/models/history
 * @requires dealer-core/lib/constants
 */

var guid = require( 'hive-stdlib/random' ).guid
  , History = require("dealer-core/models/history")
  , constants = require('dealer-core/lib/constants')
  ;

// generates, shuffles, scores a new deck of cards
exports.get_list = {
	method:'get',
	route:"/deck",
	handler: function( req, res, next ){
		var session_id = req.cookies.dealer_session_id.split(/s:([\w-]+)\./)[1]
		var deck = new History({
			session_id: session_id 
		});
		deck.shuffle();
		deck.calculate();
		deck.save( function( err, d ){

			History
				.aggregate([
					{$match:{session_id:session_id}},
					{$group: {_id:"$session_id", score: {$avg:"$score_pct" } }},
					{$sort:{created:1}}
				], function( err, data ){
					 var _d = d.toObject();
					 _d.average = data[0].score
					res.send( _d )	
				})
		})
	}
}

// return a previously played deck
exports.get_detail = {
	method:'get'
	,route:'/deck/:id([a-z0-9]{24})'
	,handler: function( req, res, next ){
		History
			.getById( req.param('id') )
			.lean()
			.exec( function( obj ){
				if( err ){
					return res
						.status( 500 )
						.send({
							success:false,
							status:500
							,message: err.message
						})
				}

				if( !obj ){
					return res
						.status( 404 )
						.send({
							success: false
							,status: 404
							, message: 'History not found'
						});
				}

				res.send( obj )
			})
	}
};

// returns all decks of a specific session
exports.get_history_list = {
	method:'get',
	route:'/deck/history/:session_id([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})',
	handler: function( req, res, next ){
		History
			.find({session_id:req.param('session_id')})
			.lean()
			.exec(function( err, results ){
				res.send({
					success:true
					,data: results || []
				})
		})
	}
}
