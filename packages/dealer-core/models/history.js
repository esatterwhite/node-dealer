/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Record of decks of cards dealt
 * @module dealer-core/models/history
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires mongoose
 * @requires dealer-core/lib/db
 * @requires dealer-core/lib/constants
 * @requires dealer-core/lib/deck
 * @requires hive-stdlibe/lang
 */

var Schema    = require( 'mongoose' ).Schema
  , db        = require( '../lib/db' ).connection
  , clone     = require('hive-stdlib/lang').clone
  , constants = require('../lib/constants')
  , deck      = require('../lib/deck')
  , History
  ;

/**
 * @alias module:dealer-core/modles/history
 * @param {Object} fields
 * @param {Array} [fields.cards] Array of cards to play
 * @param {Number} fields.score=null Final score of deck - auto calculated
 * @param {String} fields.session_id A session UUID
 * @param {Date} [fields.created] auto gerated creation date
 */
History = new Schema({
	cards:{type:Array, default:function(){
		return deck.shuffle( clone( constants.cards ) );
	}}
	,score:{type:Number, min:0, max:104, default:null}
	,session_id:{type:String, default:null, index:true}
	,score_pct:{ type:Number, default:null, index:true }
	,created:{
		type:Date
		, default: function(){
			return new Date();
		}
	}
},{collection:'dealer_history'})

/**
 * shuffles current deck of cards 
 * @method module:dealer-core/models/history#shuffle
 **/
History.methods.shuffle = function(){
	this.cards = deck.shuffle( this.cards );
	this.score = null;
};

/**
 * Calculates the score for the current deck of cards
 * @chainable
 * @method module:dealer-core/models/history#calculate
 **/
History.methods.calculate = function(){

	this.score = deck.calculate( this.cards )
	return this;
};

History.pre('save', function( next ){
	// make sure everything is kosher before we save...
	this.score = this.score == null ? deck.calculate( this.cards ) : this.score;
	this.score_pct = ( this.score / 104 ) * 100;
	next();
});

module.exports = db.dealer.model('History', History)
