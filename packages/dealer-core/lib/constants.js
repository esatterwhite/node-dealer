/*jshint laxcomma: true, smarttabs: true, node: true*/
'use strict';
/**
 * Constant values for the dealer module
 * @module dealer-core/lib/constants
 * @author Eric Satterwhite
 * @since 0.0.1
 */

var len
  , cards = []
  , faces = {
		"0":"A",
		"10":"J",
		"11":"Q",
		"12":"K",
	}

/**
 * @readonly
 * @property {Array} suites Static listing of possible suits in a deck of cards
 **/
exports.suits = ['S', 'D', 'H', 'C']

len = exports.suits.length;

while( len-- ){
	for( var y=0; y < 13; y++){
		cards.push({
			suit: exports.suits[len],
			value: faces[y] ? faces[y] : 0 + y,
			valuematch: false,
			suitmatch: false
		});
	}
}

/**
 * @readonly
 * @property {Array} cards A reference deck of cards in perfect order
 **/
exports.cards = cards;
