/*jshint node: true, laxcomma: true, smarttabs: true*/
/*globals module,process,require,exports,__dirname,__filename */
'use strict';
/**
 * General database connection handler
 * @module dealer-core/lib/db/connection
 * @author Eric Satterwhite
 * @since 0.1.0
 * @requires util
 * @requires events
 * @requires mongoose
 * @requires debug
 * @requires hive-conf
 * @requires hive-log
 * @requires hive-stdlib/class
 * @requires hive-stdlib/class/options
 * @requires hive-stdlib/lang
 */

var mongoose        = require('mongoose')
  , util            = require('util')
  , url             = require('url')
  , events          = require('events')
  , conf            = require('hive-conf')
  , logger          = require('hive-log')
  , debug 		    = require('debug')('dealer-core:db:connection')
  , clone           = require('hive-stdlib/lang').clone
  , Class           = require('hive-stdlib/class')
  , Options         = require('hive-stdlib/class/options')
  , options         = null
  , Connection
  ;

mongoose.set('debug', !!conf.get('mongoose:debug'));

/**
 * Represents a connection to a datastore. May contain reference to multiple databases
 * @constructor
 * @alias module:hive-core/lib/db/connection
 * @param {Object} [options] Connection instance options
 * @param {String} [options.default=null] the name of the database to treat as the default
 * @param {Boolean} [options.autoload=true] try to reconnect if disconnected
 * @param {Object} [options.databases] An object defining the set of databases to connect to defaults to the
 */
Connection = new Class({
	inherits:events.EventEmitter
	,mixin: Options
	,options:{
		default: null
		,autoload: true
		,databases: {}
	}
	,constructor: function( options ){
		var str
		  , fn
		  , that = this
		  , cache = {}
		  , databases
		  ;

		this.setOptions( options );

		databases = this.options.databases;
		Object
			.keys( databases )
			.forEach(function( db ){

				logger.info( 'connection to %s database', db );

				cache[ db ] = this.connect( db, databases[ db ]);
				cache[ db ].on('error', this.onError.bind( this, db ) );

				// enable the default
				if( databases[db].default ){
					that.setOptions({default: db });
				}

				Object.defineProperty(this, db,{
					configurable: false
					,enumberable: false
					,get: function( ){
						return cache[ db ];
					}
				});

				/**
				 * @name module:alice-core/lib/db/connection#db:connect
				 * @event
				 * @param {String} name description
				 * @param {Connection} name description
				 **/
				this.emit('db:connect', db, cache[db]);

			}.bind( this ) );
			Object.defineProperty(this, 'default',{
				configurable: false
				,enumberable: false
				,get: function( ){
					return cache[ this.options.default ];
				}
				,set:function( name ){
					if( cache.hasOwnProperty( name ) ){
						this.setOptions({default: name });
						return cache[ this.options.default ];
					}
				}
			});
	}

	/**
	 * connects to a specific
	 * @method module:alice-core/lib/db/connection#disconnect
	 * @param {String} name name of the database connection
	 * @param {Object} options
	 * @returns {database} mogodb database object
	 **/
	, connect: function( name, opts ){
		opts = opts || this.options.databases[ name ]
		var str = this.format( name, clone( opts ) )
		  , that = this
		  , conn
		  ;


		if( this.hasOwnProperty( name ) ){
			return logger.error('Connection %s already exists', name );
		}

		/**
		 * @name connection.js.Thing#event
		 * @event
		 * @param {TYPE} name the name of the db about to be connected to
		 **/
		this.emit( 'before:db:connect', name );
		logger.debug("connecting to %s", str );
		conn = mongoose.createConnection( str );
		conn.on('open', function onOpen(){
			that.emit('db:connection', name, that[ name ] );
		});

		[
			'disconnected'
		  , 'connected'
		  , 'connecting'
		  , 'disconnecting'
	    ].forEach( function( state ){
	    	logger.debug('adding db %s event handler for %s ', name, state )
		    conn.on(state, function( ){
			    logger.debug('%s state: %s ', name, state);
			    that.emit( util.format( '%s:%s', name, state ),name, state );
		    });
		});

		return conn;
	}

	/**
	 * disconnects from a specific database
	 * @chainable
	 * @method module:alice-core/lib/db/connection#disconnect
	 * @param {String} name The name of the db to disconnect from
	 * @returns {Connection} Current connection instance
	 **/
	, disconnect: function( name ){
		this[ name ].close();
		this[ name ].removeAllListeners();
		return this;
	}

	/**
	 * Generates a valid connection string for the configured data store
	 * @method module:alice-core/lib/db/connection#format
	 * @param {String} name
	 * @param {Object} options
	 * @return {String} a connection uri string
	 **/
	, format: function format( name,  options ){
		// if it is a connection string, just return it.
		if( typeof options == 'string' ){
			return options;
		}
		options.pathname = options.pathname || name;
		options.protocol = options.driver;
		options.hostname = options.host;
		options.slashes  = true;
		delete options.host;

		options.auth = (options.user && options.password) ?
			util.format('%s:%s', options.user, options.password) :
			undefined;

		return url.format( options );
	}

	, onError: function onError( name, error ){
		if( !this.hasOwnProperty( name ) ){
			return;
		}

		logger.error('database error [%s] %s', name, error.message );
		logger.warning('shutting down %s', name);
		this.emit('error', error );

		this
			.disconnect( name )
			.connect( name );

	}
});

module.exports = Connection;
