/*jshint laxcomma: true, smarttabs: true*/
'use strict';
/**
 * exposes connected databases
 * @module hive-core/lib/db
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires dealer-core/lib/db/connection
 */

var Connection = require( './connection' )
  , conf       = require('hive-conf')
  , clone      = require('hive-stdlib/lang').clone
  ;

exports.Connection = Connection;
exports.connection = new Connection( {databases:clone( conf.get('dealer:databases') )} );

