/*jshint laxcomma: true, smarttabs: true, node: treu*/
'use strict';
/**
 * dealder-core/lib/deck
 * @module dealer-core/lib/deck
 * @author Eric Satterwhite
 * @since 0.0.1
 * @requires hive-stdlib/random/randInt
 * @requires hive-stdlib/operator
 * @requires dealer-core/lib/constants
 */

var random    = require( 'hive-stdlib/random' ).randInt
  , operator  = require('hive-stdlib/operator')
  , constants = require('./constants')
  , counter   = 0
  , ops
  ;

ops = [ operator.lt, operator.gt ]

/**
 * Randomly sorts an array in alternating be tween asc and desc
 * @param {Array} cards A deck of cards to shuffle
 * @return {Array}
 **/
exports.shuffle = function( cards ){

	cards.sort( function(){
		return ops[ ++counter % ops.length ]( Math.random(), Math.random() ) ? 1 : -1
	});
	return cards;
};


/**
 * Performs deck socring and pair finding
 * @param {Array} cards An array of cards to calculate a core
 * @return {Number} score
 **/
exports.calculate = function( cards ){
 	var score = 0
	  , y     = 0
	  , size  = 13
	  , suit  = 0
	  , idx   = 0
	  , len   = constants.suits.length
	  , ref
	  , current
	  , prev
	  ;
	  

	while( suit < len ){
		idx =  (suit * size + y );
		ref = constants.cards[idx];
		current = cards[idx];
		prev = cards[idx - 1]

		current.suitmatch = current.valuematch = current.match = false;

		if( current.suit == ref.suit ){
			current.suitmatch = true
			++score;
		} 

		if( ref.value == current.value ){
			current.valuematch = true;
			++score;
		}

		if( prev && y && current.value == prev.value ){
			current.match = prev.match = true;
		};

		y = ( y + 1 ) % size;
		suit = !y ? suit + 1 : suit;
	}
	return score;
};
